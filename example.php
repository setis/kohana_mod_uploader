<?php

class Controller_Mediastorage_Upload {

    const
            file = 0,
            video = 1,
            image = 2,
            document = 3;

    static $nameTypes = [
        self::file => 'file',
        self::video => 'video',
        self::image => 'image',
        self::document => 'document'
    ];

    public function upload($file, $type) {
        if (isset(self::$nameTypes[$type])) {
            $cfg = self::$nameTypes[$type];
        } elseif(in_array($file, self::$nameTypes)){
            
        }else {
            $cfg = self::$nameTypes[self::file];
        }
        return self::uploading($file,$cfg);
    }

    static function uploading($file,$cfg) {
        if (!isset($_FILES[$file])) {
            throw new Exception('не указен файл ' . $file);
        }
        $uploader = Uploader::instance($cfg);
        return $uploader->exec($_FILES[$file]);
    }

     static function form($file,$cfg) {
        $html = '<form enctype="multipart/form-data" method="post" action="/api/?mod=' . __CLASS__ . '&action=upload&in[]=' . $file . '&in[]=' . $cfg . '">
                        <input type="file" size="32" name="' . $file . '" value="">
                        <input type="submit" name="Submit" value="upload">
                    </form>';
        echo $html;
        exit();
    }

}