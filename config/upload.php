<?php

defined('SYSPATH') OR die('No direct script access.');

return array(
    'user' => array(
        'lang' => 'ru_RU',
        'image_resize' => true,
        'image_x' => 100,
        'image_ratio_y' => true,
        'dir' => '/var/www/uploads',
        'upload_max_filesize' => '10M'
    ),
    'logo' => array(
        'lang' => 'ru_RU',
        'image_resize' => true,
        'image_x' => 100,
        'image_ratio_y' => true,
        'dir' => '/var/www/uploads/logo',
        'allowed' => array("image/*"),
        'file_is_image' => true,
        'upload_max_filesize' => '10M'
    ),
    'avatar' => array(
        'lang' => 'ru_RU',
        'image_resize' => true,
        'image_x' => 100,
        'image_ratio_y' => true,
        'image_convert' => 'jpg',
        'file_overwrite' => true,
        'allowed' => array("image/*"),
        'file_is_image' => true,
        'dir' => '/var/www/uploads/avatar',
        'upload_max_filesize' => '10M'
    ),
    'image' => [
        'lang' => 'ru_RU',
        'image_resize' => true,
        'image_x' => 100,
        'image_ratio_y' => true,
        'dir' => '/var/www/uploads',
        'upload_max_filesize' => '4M',
        'allowed' => ['image/*'],
        'image_convert' => 'jpg',
        'jpeg_quality' => 50
    ],
    'video' => [
        'lang' => 'ru_RU',
        'dir' => '/var/www/uploads',
        'upload_max_filesize' => '4G',
//        'allowed' => ['video/*'],
        'trigger' => [
            Uploader::tigger_before => function($uploader) {
                $uploader->api()->file_new_name_body = uniqid($uploader->use . '_');
            },
            Uploader::trigger_after => function($uploader) {
                $api = $uploader->api();
                $path = $api->file_dst_pathname;
                /**
                 * @todo полчение информации об видео
                 * @param type file
                 * @return type array
                 */
               
                if ($uploader->use === 'video') {
                    $info['video'] = Uclass::movieInfo($path);
                } else {
                    $info['image'] = [
                        'width' => $api->image_src_x,
                        'height' => $api->image_src_y,
                        'type' => $api->image_src_type,
                        'bits' => $api->image_src_bits,
                        'pixels' => $api->image_src_pixels,
                    ];
                }

                $info['file']['type'] = $uploader->use;
                $info['file']['size'] = filesize($path);
                $info['file']['md5'] = md5_file($path);
                $ctime = filectime($path);
                $info['file']['ctime']['unixstamp'] = $ctime;
                $info['file']['ctime']['format'] = date('j-m-Y H:i:s', $ctime);
                $info['file']['name']['new'] = $api->file_dst_name;
                $info['file']['name']['old'] = $api->file_src_name;
                $file = strstr($path, pathinfo($path, PATHINFO_EXTENSION), true) . 'json';
                file_put_contents($file, json_encode($info));
                $info['path']= $path;
                $info['result'] = true;
                return $info;
            },
        ]
    ]
);
?>
